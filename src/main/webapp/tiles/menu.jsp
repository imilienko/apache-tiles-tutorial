<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ page contentType="text/html; UTF-8" %>

<aside >
    <ul>
        <li><a href="/item_one">Item one</a></li>
        <li>
            <a href="/item_two">
                Item two
            </a>
        </li>
        <li>
            <a href="/path/to/item_three">
                Item three
            </a>
        </li>
    </ul>
</aside>
